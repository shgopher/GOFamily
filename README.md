<p align="left">
hey~，我是科科人神，目前就职于国内一家互联网公司，你们可以加我<a href="#wechat.png">微信</a>，交个朋友吧~
</p>
<br>
<p align="center">
<a href='#wechat.png'
 target="_blank"><img src="https://img.shields.io/static/v1?label=%E7%A7%91%E7%A7%91%E4%BA%BA%E7%A5%9E&message=%E5%85%AC%E4%BC%97%E5%8F%B7&color="></a>
<a href="https://space.bilibili.com/478621088" target="_blank"><img src="https://img.shields.io/static/v1?label=bilibili&message=b%E7%AB%99&color=blue"></a>
<a href="https://www.zhihu.com/people/shgopher" target="_blank"><img src="https://img.shields.io/static/v1?label=zhihu&message=%E7%9F%A5%E4%B9%8E&color=blue"></a>
<a href="https://blog.csdn.net/zyfljxzby" target="_blank"><img src="https://img.shields.io/static/v1?label=csdn&message=CSDN&color=red"></a>
<a href="https://www.toutiao.com/c/user/token/MS4wLjABAAAAIGeO1-kCUelF-G8GW3AvJlrEL7tiO24WHJmnX4nV1bs" target="_blank"><img src="https://img.shields.io/static/v1?label=toutiao&message=%E5%A4%B4%E6%9D%A1&color=red"></a>
</p>

添加微信公众号：<a href="#wechat.png">科科人神</a>，回复：
- `面试题`，获取经典 go 面试大全 (巨全，强推，不仅有 go 面试题还有大厂的算法和系统设计原题以及面试技巧等等内容)

- `好友`，可以添加作者的微信好友

## 进度
>以日常开发的角度进行评估重要性

|类别|进度|重要性|
|:--:|:--:|:--|
|基础|100%|⭐️⭐️⭐️⭐️⭐️|
|并发|30%|⭐️⭐️⭐️⭐️⭐️|
|runtime|0%|⭐️⭐️|
|编译|10%|⭐️|
|项目实践|30%|⭐️⭐️⭐️⭐️⭐️|
                             
## 基础
- [变量声明](./基础/变量声明)
- [常量声明](./基础/常量声明)
- [零值](./基础/零值)
- [复合字面量](./基础/复合字面量)
- [数字类型](./基础/数字类型)
- [结构体](./基础/结构体)
- [slice](./基础/slice)
- [string](./基础/string)
- [map](./基础/map)
- [求值顺序](./基础/求值顺序)
- [作用域](./基础/作用域)
- [函数方法](./基础/函数方法)
- [接口](./基础/interface)
- [逻辑和判断语句](./基础/逻辑和判断语句) 
- [泛型](./基础/泛型)
- [其他内容](./基础/其他内容)
## 并发
- [内存模型](./并发/内存模型)
- [并发模型](./并发/并发模型)
- [同步原语](./并发/同步原语)
- [channel](./并发/channel)
- [context](./并发/context)
- [定时器](./并发/定时器)
- [atomic](./并发/atomic)
## runtime
- [三色 gc 算法](./runtime/三色gc算法)
- [G:M:P](./runtime/gmp)
- [堆内存分配](./runtime/堆内存分配)
- [栈内存管理](./runtime/栈内存管理)
- [系统监控](./runtime/系统监控)
- [netpool](./runtime/netpool)
## 编译器
- [gc](./编译器/gc)
- [llvm](./编译器/llvm)
- [gccgo](./编译器/gccgo) 
## 工程
- [go 编程范式/设计模式](./工程/go编程范式/README.md)
- [包管理工具](./工程/包及其构建工具)
- [log 包](./工程/log)
- [测试](./工程/测试)
- [动态调试](./工程/动态调试)
- [错误处理](./工程/错误处理)
- [cgo](./工程/cgo)
- [代码检查](./工程/代码检查)
- [反射](./工程/反射)
- [go web 编程](./工程/goweb编程)
- [wasm](./工程/wasm)
- [ioc](./工程/ioc)
- [命令](./工程/命令)
- [字符编码](./工程/字符编码)
- [系统信号的处理](./工程/系统信号的处理)
- [go 语言的密码学](./工程/go语言的秘密学)
- [优秀第三方包](./工程/优秀第三方包)
- [go 标准库](./工程/go标准库)
- [go 项目组织形式](./工程/项目组织形式)
- [go 命名惯例](./工程/go命名惯例)
- [go 语言规范](./工程/go语言规范)
- [go 面试题](./工程/go面试题)
## 更多内容
> 👷 正在施工中...

- [408](https://github.com/shgopher/408) 关于 408 的相关知识，例如算法数据结构，网络，操作系统，数据库等。
- [luban](https://github.com/shgopher/luban) 系统设计相关内容，例如分布式共识算法，消息队列，容器，RPC 等内容。
- [dingdang](https://github.com/shgopher/dingdang) 各种工具类的使用，例如 git，正则表达式，ddd，CI/CD 等内容。
- [god](https://github.com/shgopher/god) 关于程序员自身的一些看法，例如面试经历，赚钱窍门，创业门道，如何向上
管理等。
- [RustFamily](https://github.com/shgopher/RustFamily) rust 基础知识，rust 并发编程，rust 项目实践，rust 底层原理。
## 参考资料
- [go.dev](https://go.dev)
- [go src](https://github.com/golang/go)
- [the go programming language](https://www.gopl.io/)
- [GO 语言精进之路](https://book.douban.com/subject/35720729/)
- [Mastering Go](https://shgopher.github.io/pdf/mastering-go-cn.pdf)
- [Go 语言设计与实现](https://draveness.me/golang/)
- [深度探索 GO 语言](https://book.douban.com/subject/36104087/)
- [我为什么放弃 GO 语言？](https://juejin.cn/post/7241452578125824061)
- [100-go-mistakes](https://github.com/teivah/100-go-mistakes)
- [Go 语言编程模式实战](https://time.geekbang.org/opencourse/intro/100069501)
- [Go 并发编程实战课](https://time.geekbang.org/column/intro/100061801)
- [Go 语言项目开发实战](https://time.geekbang.org/column/intro/100079601)
- [Go 语言核心 36 讲](https://time.geekbang.org/column/intro/100013101)
- [手把手带你写一个 Web 框架](https://time.geekbang.org/column/intro/100090601)
- [Google 资深工程师深度讲解 Go 语言](https://coding.imooc.com/class/chapter/180.html#Anchor)
- [Golang-Internal-Notes](https://github.com/LeoYang90/Golang-Internal-Notes)

## 扫一扫添加我的公众号，回复 “加群”，可以加入微信群。

<p id="wechat.png" align="center">
<br>
<br>
<img src="./wechat.png"  alt="公众号搜：科科人神">
</p>

## 实战项目推荐

光说不练假把式，学习一门编程语言还是得上实战！

我本人买了很多的课程用来进阶 go 语言。

我认为，**练习项目**是最容易提升的方法。

这个课程**难度适中**，既不是特别复杂的项目，也不是那种浅显的项目，它使用**微服务**的架构去讲解实际业务中的开发经验，并且讲解的很细致。

这个课程是我认为，难度适中，内容丰富，价格也相对亲民，有需求的可以扫码购买。

这个课程我至少看了三遍，强烈推荐，作者是**前腾讯资深开发，现字节云业务某负责人**，人非常的不错，我推荐这个实战项目给看完了理论知识的大家！

![ad1](./ad1.jpg)
## star
                                                                             
[![Stargazers over time](https://starchart.cc/shgopher/GOFamily.svg)](https://starchart.cc/shgopher/GOFamily)

## 证书
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="知识共享许可协议" style="border-width:0" src="https://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br />本作品采用<a rel="license" href="http://creativecommons.org/licenses/by/3.0/">知识共享署名 3.0 未本地化版本许可协议</a>进行许可。
